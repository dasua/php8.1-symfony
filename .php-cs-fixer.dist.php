<?php

declare(strict_types=1);

// https://mlocati.github.io/php-cs-fixer-configurator/
$finder = PhpCsFixer\Finder::create()
    ->in([__DIR__, 'src', 'public'])
    ->exclude(['.docker', 'bin', 'config', 'var', 'vendor']);

return (new PhpCsFixer\Config())
    ->setRules([
        '@Symfony' => true,
        'array_indentation' => true,
        'align_multiline_comment' => true,
        'blank_line_before_statement' => [
            'statements' => ['for', 'foreach', 'if', 'return', 'switch', 'while', 'yield'],
        ],
        'method_chaining_indentation' => true,
        'native_function_invocation' => [
            'include' => ['@compiler_optimized'],
            'scope' => 'namespaced',
        ],
        'class_attributes_separation' => [
            'elements' => ['method' => 'one', 'property' => 'one', 'trait_import' => 'one']
        ],
    ])
    ->setFinder($finder)
    ->setCacheFile(__DIR__.'/.php-cs-fixer.cache')
    ->setRiskyAllowed(true)
    ;
