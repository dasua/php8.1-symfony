# Instalación y configuración inicial

El primero paso es descargar el repositorio y descomprimirlo en un directorio.

El requisito para poder utilizar este repositorio es tener instalado y funcionando docker, ya que está pensado para no
tener instalado en local PHP, composer, npm, etc.

Disponemos del comando `make` para poder crear los contenedores según las necesidades del proyecto.

## Comando make

El comando `make` está disponible para Linux y Mac desde los repositorios. Para instalarlo en Windows puedes
utilizar [Chocolatey](https://chocolatey.org/):

[How to install and use "make" in Windows?](https://stackoverflow.com/questions/32127524/how-to-install-and-use-make-in-windows).

## Operaciones sobre los contenedores

### Crear los contenedores

```shell
make build
```

### Iniciar los contenedores

```shell
make start
```

### Parar los contenedores

```shell
make stop
```

### Lista de comandos disponibles

```shell
make help
```

Para generar las imágenes del proyecto, debemos realizar los siguientes pasos:

- Modificaciones a realizar en `docker-compose.yml`.
    - Actualiza los nombres de los hosts, container_name, networks, volumes, etc. según necesites. Recuerda cambiar los
nombres en todos los ficheros del proyecto, no solo en **docker-compose.override.yml** y **docker-compose.override-example.yml**,
ya que algunos de los valores se utilizan para las configuraciones.
- Si quieres hacer configuraciones específicas, renombra el fichero **docker-compose.override-example.yml** a
**docker-compose.override.yml** y adáptalo a tus necesidades. Ten en cuenta que **por defecto no están exponiendo puertos http/https**.
- Abre una consola en el directorio raíz del proyecto, crea la imagen `make build`, inicia el stack `make start`.
- Comprueba que todo funciona correctamente accediendo a http://localhost (debería aparecer información de phpinfo y phpxdebug).
- Comprueba que RabbitMQ funciona correctamente accediendo a http://localhost:15672 con el usuario y password por defecto (guest).
- Si todo funciona correctamente, borra el fichero `public/index.php`.
- Descarga el composer.json del proyecto que quieres crear según la versión de Symfony:

| Versión | Comando                                                                         |
|---------|---------------------------------------------------------------------------------|
| 5.4     | ```wget https://raw.githubusercontent.com/symfony/skeleton/5.4/composer.json``` |
| 6.4     | ```wget https://raw.githubusercontent.com/symfony/skeleton/6.4/composer.json``` |
| 7.0     | ```wget https://raw.githubusercontent.com/symfony/skeleton/7.0/composer.json``` |

### Actualizaciones en el fichero composer.json

Actualiza el composer.json según las necesidades del proyecto:
  - Versión de PHP
  - Nombre del proyecto

#### Restringir la instalación de paquetes a la versión de php que hay en producción:

Para indicar a composer la versión de producción, se indica con la siguiente configuración:

```json
{
    "config": {
        "platform": {
            "php": "8.3.3"
        }
    }
}
```

#### Scripts para composer.json

Añadir al composer.json los comandos que se quieran utilizar, en función de las dependencias adicionales que se instalen
en el apartado `Software adicional`:

```json
{
    "autoload-dev": {
        "psr-4": {
            "Utils\\": "Utils/"
        }
    },
    "scripts": {
        "app:code-analyze": "vendor/bin/phpstan analyze",
        "app:code-style": "vendor/bin/php-cs-fixer fix --allow-risky=yes --verbose --show-progress=dots",
        "app:info": [
            "php --version",
            "bin/console about"
        ],
        "app:install-git-hooks": [
            "Utils\\ComposerScripts::installGitHooks"
        ]
    },
    "scripts-descriptions": {
        "app:code-analyze": "Ejecuta PHPStan para analizar el código",
        "app:code-style": "Ejecuta php-cs-fixer para formatear el código según el standard de Symfony",
        "app:info": "Muestra información de PHP y Symfony",
        "app:install-git-hooks": "Instala los hooks para Git"
    }
}
```

### Instalación de Symfony 

Instala Symfony con el comando:

```bash
composer install
```

## Acceso a la web

Comprueba que está funcionando accediendo a 

`http://localhost`

En caso de que utilices otro puerto, indícalo en la URL.

## Software adicional

Esta configuración parte de la base de que se está utilizando Doctrine. Si no es así, es posible que PHPStan de algún error.

Basta con eliminar el fichero `test/object-manager.php` y eliminar la configuración de Doctrine que hay en el fichero `phpstan.dist.neon`,

Información adicional en [Using PHPStan with Symfony - static analysis for better PHP code quality](https://accesto.com/blog/how-to-ensure-php-code-quality-static-analysis-using-phpstan-with-symfony/)

```shell
composer require --dev phpstan/phpstan \
    phpstan/extension-installer \
    phpstan/phpstan-phpunit \
    phpstan/phpstan-strict-rules \
    phpstan/phpstan-deprecation-rules \
    phpstan/phpstan-symfony \
    phpstan/phpstan-doctrine \
    symfony/test-pack \
    maker \
    friendsofphp/php-cs-fixer \
&& true
```
