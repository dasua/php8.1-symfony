# PHP8.3 Symfony

Skeleton docker para [Symfony](https://symfony.com/) con las siguientes características:

- PHP 8.3 fpm
- NGINX 1.21

## Opcional en el docker-compose.override-example.yml

- PostgreSQL 16
- RabbitMQ 3.10.2 con interfaz web

## Consideraciones

Para permitir configurar los puertos, estos son expuestos en el fichero `docker-compose.override-example.yml`.

### PHP 8.3 fpm

#### Extensiones de PHP

- amqp
- apcu
- gd
- intl
- mcrypt
- opcache
- pcntl
- pdo
- pdo_pgsql
- soap
- sockets
- xdebug
- zip

#### Software de terceros

- [Symfony CLI](https://symfony.com/download)
- [Composer](https://getcomposer.org/)
- [Node.js (LTS)](https://nodejs.org/es/)
- [Yarn](https://yarnpkg.com/)
- [ FriendsOfPHP/PHP-CS-Fixer ](https://github.com/FriendsOfPHP/PHP-CS-Fixer)

## Instalación

Ver INSTALL.md
