#!/bin/bash

UID = $(shell id -u)
DOCKER_BE = sf-php
DOCKER_FE = sf-web

help: ## Muestra la ayuda
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m\033[0m\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-15s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)

# Gestión de contenedores
start: ## Inicia los contenedores
	@U_ID=${UID} docker compose up -d

stop: ## Para los contenedores
	@U_ID=${UID} docker compose stop

down: ## Apaga los contenedores
	@U_ID=${UID} docker compose down

restart: ## Reinicia los contenedores
	@$(MAKE) stop && $(MAKE) start

build: ## Reconstruye todos los contenedores
	@U_ID=${UID} docker compose build --no-cache

ssh-be: ## Inicia bash en el contenedor de backend
	@U_ID=${UID} docker exec -it --user ${UID} ${DOCKER_BE} bash

# Gestión de dependencias
composer-install: ## Instala las dependencias de composer
	@U_ID=${UID} docker exec --user ${UID} ${DOCKER_BE} composer install --no-interaction

logs: ## Muestra los logs de Symfony
	@U_ID=${UID} docker exec -it --user ${UID} ${DOCKER_BE} symfony server:log

dump: ## Muestra la información de depuración de Symfony
	@U_ID=${UID} docker exec -it --user ${UID} ${DOCKER_BE} symfony console server:dump

yarn-watch: ## Ejecuta yarn-watch
	@U_ID=${UID} docker exec -it --user ${UID} ${DOCKER_BE} yarn watch

code-analyze: ## Ejecuta phpstan para analizar el código
	@U_ID=${UID} docker exec --user ${UID} ${DOCKER_BE} vendor/bin/phpstan analyze

code-style: ## Ejecuta php-cs-fixer para formatear el código según el standard de Symfony
	@U_ID=${UID} docker exec --user ${UID} ${DOCKER_BE} vendor/bin/php-cs-fixer fix --allow-risky=yes --verbose --show-progress=dots

info: ## Muestra información de PHP y Symfony
	@U_ID=${UID} docker exec --user ${UID} ${DOCKER_BE} php --version
	@U_ID=${UID} docker exec --user ${UID} ${DOCKER_BE} bin/console about
